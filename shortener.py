import webapp
from urllib.parse import unquote

class shortener (webapp.webApp):
    urls: dict = {}

    def parse(self,request):
        metodo = request.split(' ',2)[0]
        recurso = request.split(' ',2)[1]
        mensaje: str = unquote(request.split('\r\n\r\n', 1)[1])
        return (metodo,recurso,mensaje)

    def process(self,parsedRequest):
        (metodo,recurso,mensaje) = parsedRequest

        if metodo == "GET":
            if recurso == "/":
                form = """
                    <form action="/" method="post">
                    url: <input type = "text" name = "url">
                    short: <input type = "text" name = "short"> 
                    <input type = "submit" value = "Submit">  
                    </form> 
                 """
                htmlCode = "200 OK"
                # claves = urls.keys()
                # valores = urls.values()
                dictionary = " <p> " + str(self.urls) + "</p> "
                htmlBody = form + dictionary

            elif recurso in self.urls:
                htmlCode = "308 Permanent Redirect"
                htmlBody = '<meta http-equiv="refresh" content="1;URL=' + self.urls[recurso] + '">'

            return htmlCode, htmlBody

        if metodo == "POST":
            if not mensaje:
            # devuelve una página HTML con mensaje de error
                htmlCode = "404 Not Found"
                htmlBody = """ <p> "Se ha producido un error" </p> """
            else:
                #valor = self.urls.get(mensaje)
                c = mensaje.split('&')
                valor = c[0].split('=')[1]
                clave = c[1].split('=')[1]
                apodo = '/' + clave
                self.urls[apodo] = valor
                htmlCode = "200 OK"
                htmlBody = "<p> URL original </p>" + str(self.urls)#[clave]

            return htmlCode, htmlBody

if __name__ == "__main__":
    testWebApp = shortener("localhost", 1234)
